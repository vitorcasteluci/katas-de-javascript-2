function add(a, b) {
  return a + b;
}
document.write('Soma: ', add(2, 4));
document.write('<br>');

function multiply(a, b) {
  let m = 0;
  for (let i = 0; i < b; i++) {
    m = add(m, a);
  }
  return m;
}
document.write('Multi: ', multiply(6, 8));
document.write('<br>');

function power(a, b) {
  let pow = 1;
  for (let i = 0; i < b; i++) {
    pow = multiply(pow, a);
  }
  return pow;
}
document.write('Pow: ', power(2, 8));
document.write('<br>');

function factorial(a) {
  let fac = 0;
  for (let i = a - 1; i > 0; i--) {
    fac += multiply(a, i);
  }
  return fac;
}
document.write('Factorial: ', factorial(4));
document.write('<br>');

function fibonacci(num) {
  if (num <= 1) return 0;
  if (num === 2) return 1;

  return add(fibonacci(num - 1), fibonacci(num - 2));
}
document.write('Fibonacci: ', fibonacci(8));
document.write('<br>');
